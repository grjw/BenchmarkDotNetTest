﻿using System;
using BenchmarkDotNet.Running;

namespace BenchmarkTest
{
    class Program
    {
        static void Main(string[] args)
        {
             _ = BenchmarkRunner.Run<IfSwitchTests>();
        }
    }
}
