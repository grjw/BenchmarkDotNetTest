``` ini

BenchmarkDotNet=v0.12.0, OS=Windows 10.0.18362
Intel Core i7-4700MQ CPU 2.40GHz (Haswell), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=3.1.100
  [Host]     : .NET Core 3.1.0 (CoreCLR 4.700.19.56402, CoreFX 4.700.19.56404), X64 RyuJIT
  DefaultJob : .NET Core 3.1.0 (CoreCLR 4.700.19.56402, CoreFX 4.700.19.56404), X64 RyuJIT


```
| Method | Length |       Mean |     Error |    StdDev | Ratio | RatioSD |
|------- |------- |-----------:|----------:|----------:|------:|--------:|
| **IfElse** |   **1000** |   **2.753 us** | **0.0162 us** | **0.0144 us** |  **1.37** |    **0.01** |
|   IfOr |   1000 |   2.006 us | 0.0104 us | 0.0098 us |  1.00 |    0.00 |
| Switch |   1000 |   1.926 us | 0.0118 us | 0.0105 us |  0.96 |    0.01 |
|        |        |            |           |           |       |         |
| **IfElse** |  **10000** |  **27.062 us** | **0.1490 us** | **0.1321 us** |  **1.26** |    **0.01** |
|   IfOr |  10000 |  21.469 us | 0.1745 us | 0.1547 us |  1.00 |    0.00 |
| Switch |  10000 |  19.234 us | 0.1150 us | 0.1020 us |  0.90 |    0.01 |
|        |        |            |           |           |       |         |
| **IfElse** | **100000** | **296.557 us** | **1.8365 us** | **1.6280 us** |  **1.49** |    **0.02** |
|   IfOr | 100000 | 199.171 us | 2.2181 us | 2.0748 us |  1.00 |    0.00 |
| Switch | 100000 | 176.597 us | 2.6202 us | 2.4509 us |  0.89 |    0.02 |
