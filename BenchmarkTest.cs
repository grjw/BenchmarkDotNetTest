using BenchmarkDotNet.Attributes;

namespace BenchmarkTest
{

	public class IfSwitchTests 
	{
		[Params(1000, 10000, 100000)]
		public int Length;
		public int[] data;

		[GlobalSetup]
		public void Setup()
		{
			data = new int[Length];
		}

		[Benchmark]
		public void IfElse()
		{
			for (int i = 0; i < Length; i++)
			{
				var num = i % 10;
				if (num == 2)
				{
					data[i] = i;
				}
				else if (num == 4)
				{
					data[i] = i;
				}
				else if (num == 6)
				{
					data[i] = i;
				}
				else if (num == 8)
				{
					data[i] = i;
				}
				else if (num == 0)
				{
					data[i] = i;
				}
			}
		}

		[Benchmark(Baseline = true)]
		public void IfOr()
		{
			for (int i = 1; i < Length; i++)
			{
				var num = i % 10;

				if (num == 2 || num == 4 || num == 6 || num == 8 || num == 0)
				{
					data[i] = i;
				}
			}
		}
	
		[Benchmark]
		public void Switch()
		{
			for (int i = 1; i < Length; i++)
			{
				var num = i % 10;

				switch (num)
				{
					case 2:
					case 4:
					case 6:
					case 8:
					case 0:
						data[i] = i;
						break;
				}
			}
		}
	}

}